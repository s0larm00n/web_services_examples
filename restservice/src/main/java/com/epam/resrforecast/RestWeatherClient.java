package com.epam.resrforecast;

import com.epam.weathergetter.model.WeatherData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class RestWeatherClient {
    public static void main(String[] args) {
        try {

            Client client = Client.create();

            WebResource webResource = client
                    .resource("http://localhost:2626/bar/weather/param?name=Ufa&temp=true");

            ClientResponse response = webResource.accept("application/json")
                    .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            String output = response.getEntity(String.class);

            Gson gson = new Gson();
            WeatherData res = gson.fromJson(output,WeatherData.class);

            System.out.println("Output from Server:");
            System.out.println(res.getCity()+' '+res.getTemp());

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
