package com.epam.resrforecast;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import com.epam.weathergetter.controller.Controller;
import com.epam.weathergetter.model.Initializer;
import com.epam.weathergetter.model.WeatherData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/weather")
public class RestWeatherService {

    @GET
    @Path("/param")
    public String getMsg(@QueryParam("name") String cityName,
                           @QueryParam("temp") boolean tempTypeF) {


        Controller controller=new Controller();
        ArrayList<WeatherData> dataBase= Initializer.initialize();
        //showBase(dataBase);
        WeatherData weatherData = controller.getTempObj(cityName,dataBase,tempTypeF);

        //String output = "Jersey say : " + weatherData.getCity()+' '+weatherData.getTemp()+' '+cityName+' '+tempTypeF;

        Gson gson = new GsonBuilder().create();
        return gson.toJson(weatherData);

        //return Response.status(200).entity(weatherData).build();

        /*Response.ResponseBuilder response = Response.ok((Object)weatherData);
        response.header("The response","What is this?");
        return response.build();*/

    }

}