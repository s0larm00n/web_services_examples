package com.epam.resrforecast;

import org.glassfish.jersey.server.ResourceConfig;

public class ResourceConfiguration extends ResourceConfig {
        public ResourceConfiguration() {
            packages("com.epam.resrforecast");
            register(RestWeatherService.class);
        }

    }
