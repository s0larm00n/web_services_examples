package com.epam.weathergetter.controller;

import com.epam.weathergetter.model.WeatherData;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class Controller {
    public static WeatherData getTemp(String inputCity, ArrayList<WeatherData> dataBase){
        Integer temp=findTemp(dataBase,inputCity);
        String city=inputCity;
        WeatherData res=null;
        try {
            if(temp==null){
                throw new NoSuchElementException("Unknown city");
            }
        }
        catch(NoSuchElementException e){
            city="Unknown city! The temp in Bikini Bottom is";
            temp= 20;
        }
        finally {
            res=new WeatherData(city,temp);
        }
        return res;
    }

    static Integer findTemp(ArrayList<WeatherData> dataBase,String cityName){
        if(!dataBase.isEmpty()) {
        ListIterator<WeatherData> listIter = dataBase.listIterator();
            while (listIter.hasNext()) {
                WeatherData step = listIter.next();
                if (step.containsCityName(cityName)) {
                    return step.getTemp();
                }
            }
        }
        return null;
    }

    public static int fromCtoF(int input){
        return (int)((float)9/5*input+32);
    }

    public WeatherData getTempObj(String inputCity, ArrayList<WeatherData> dataBase, boolean tempTypeF){
        WeatherData res=null;
        Integer temp=Controller.findTemp(dataBase,inputCity);
        String city=inputCity;
        try{
            if(tempTypeF!=true&&tempTypeF!=false){
                throw new IllegalArgumentException("Unknown temp format");
            }
            if(temp==null){
                throw new NoSuchElementException("Unknown city");
            }
            if(tempTypeF){
                temp=fromCtoF(temp);
            }
        }catch(IllegalArgumentException e){
            city="Unknown temperature type! Random number";
            temp=666;
        }
        catch(NoSuchElementException e){
            city="Unknown city! The temp in Bikini Bottom is";
            temp=666;
        }
        finally {
            res=new WeatherData(city,temp);
        }
        return res;
    }
}