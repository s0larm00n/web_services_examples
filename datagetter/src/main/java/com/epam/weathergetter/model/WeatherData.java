package com.epam.weathergetter.model;

import java.io.Serializable;

public class WeatherData implements Serializable {
    private String city;
    private int temp;
    //Temperature stored as C by default

    public WeatherData(String newCity, int newTemp){
        this.city=newCity;
        this.temp=newTemp;
    }

    public WeatherData(){

    }

    public String getCity(){
        return city;
    }

    public int getTemp(){
        return temp;
    }

    public void setCity(String str){
        city=str;
    }

    public void setTemp(int i){
        temp=i;
    }

    public boolean containsCityName(String str){
        return city.equals(str);
    }
}
