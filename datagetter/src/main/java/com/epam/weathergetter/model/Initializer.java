package com.epam.weathergetter.model;

import java.util.ArrayList;

public class Initializer {
    private static String []cities={"Moscow",         "Novosibirsk",         "Nizhny novgorod",   "Chelyabinsk",     "Samara",
                                     "Ufa",           "Saint Petersburg",    "Yekaterinburg",     "Kazan",           "Omsk",
                                     "Rostov-on-Don", "Krasnoyarsk"
    };

    private static int []tempsC={   -7,              -10,                   -6,                     -6,             -3,
                                    -5,              -5,                    -3,                     -2,             -15,
                                     0,              -10
    };
    public static ArrayList<WeatherData> initialize(){
        ArrayList<WeatherData> res=new ArrayList<>();
        for(int i=0;i<cities.length;i++){
            WeatherData newData = new WeatherData(cities[i],tempsC[i]);
            res.add(newData);
        }
        return res;
    }
}
