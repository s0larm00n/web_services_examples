package com.epam.weathergetter.controller;

import com.epam.weathergetter.model.WeatherData;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ControllerTest {

    @Mock
    private ArrayList<WeatherData> weatherDataListMock;

    //@InjectMocks


    String corruptedCity;
    boolean corruptedChar;
    WeatherData someData;
    String correctCityName;
    boolean correctTempType;
    Controller controller;

    @Before
    public void init() {
        controller =new Controller();

        someData=new WeatherData("Ufa",999);

        weatherDataListMock=new ArrayList<>();
        weatherDataListMock.add(someData);

        corruptedCity="sejdnkwejhnfdj";
        corruptedChar=false;
        correctCityName="Ufa";
        correctTempType=true;
    }

    @Test
    public void getTempObjReturnsWeatherData() {
        WeatherData res=controller.getTempObj(correctCityName,weatherDataListMock,true);
       assertThat(res,is(instanceOf(WeatherData.class)));
    }

    @Test
    public void getTempObjReturnsCorrectAnswer() {  //+ test for fromCtoF method
        WeatherData res=controller.getTempObj("Ufa",weatherDataListMock,true);
        assertThat(res.getTemp(),is(Controller.fromCtoF(999)));
    }


    @Test
    public void getTempObjNonexistentCityReturnsBikiniBottom() {
        WeatherData res=controller.getTempObj(corruptedCity,weatherDataListMock,correctTempType);
        assertThat(res.getCity(),is("Unknown city! The temp in Bikini Bottom is"));
    }
}