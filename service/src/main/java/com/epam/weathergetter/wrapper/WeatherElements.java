package com.epam.weathergetter.wrapper;

import javax.xml.bind.annotation.XmlElement;

public class WeatherElements {
    @XmlElement
    public String  cityName;
    @XmlElement
    public Integer temperature;

    //private WeatherElements() {} //Required by JAXB
    public WeatherElements(String cityName, Integer temperature) {
        this.cityName=cityName;
        this.temperature=temperature;
    }

    public WeatherElements() {
    }
}