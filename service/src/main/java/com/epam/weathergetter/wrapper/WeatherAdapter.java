package com.epam.weathergetter.wrapper;

import com.epam.weathergetter.model.WeatherData;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class WeatherAdapter extends XmlAdapter<WeatherElements, WeatherData> {
    public WeatherElements marshal(WeatherData arg0) {
        WeatherElements wElements = new WeatherElements(arg0.getCity(),arg0.getTemp());
        return wElements;
    }

    public WeatherData unmarshal(WeatherElements arg0) {
        WeatherData d = new WeatherData(arg0.cityName,arg0.temperature);
        return d;
    }
}