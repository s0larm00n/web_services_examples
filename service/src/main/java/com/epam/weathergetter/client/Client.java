package com.epam.weathergetter.client;

import com.epam.weathergetter.model.WeatherData;
import com.epam.weathergetter.service.IService;
import com.epam.weathergetter.wrapper.WeatherAdapter;
import com.epam.weathergetter.wrapper.WeatherElements;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public class Client{

    public static void main(String[] args) throws Exception {

        URL url = new URL("http://localhost:9999/weathergetter/service?wsdl");
        QName qname = new QName("http://service.weathergetter.epam.com/", "ServiceImplService");
        Service service = Service.create(url, qname);
        IService serv = service.getPort(IService.class);

        WeatherElements res=serv.getObjectTempByCityName("Ufa",true);
        WeatherAdapter weatherAdapter=new WeatherAdapter();
        WeatherData finalio=weatherAdapter.unmarshal(res);
        System.out.println(finalio.getTemp()+" "+finalio.getCity());

    }

}