package com.epam.weathergetter.runner;

import com.epam.weathergetter.service.ServiceImpl;

import javax.xml.ws.Endpoint;

public class Publisher {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9999/weathergetter/service", new ServiceImpl());
    }
}
