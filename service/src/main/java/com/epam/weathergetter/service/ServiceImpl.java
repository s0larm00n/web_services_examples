package com.epam.weathergetter.service;

import com.epam.weathergetter.controller.Controller;
import com.epam.weathergetter.model.Initializer;
import com.epam.weathergetter.model.WeatherData;
import com.epam.weathergetter.wrapper.WeatherAdapter;
import com.epam.weathergetter.wrapper.WeatherElements;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;

@WebService(endpointInterface = "com.epam.weathergetter.service.IService")
public class ServiceImpl implements IService {

    @WebMethod(operationName = "forecast")
    public WeatherElements getObjectTempByCityName(String cityName, boolean tempTypeF) {
        System.out.println("Building a forecast... ");
        Controller controller=new Controller();
        ArrayList<WeatherData> dataBase= Initializer.initialize();
        //showBase(dataBase);
        WeatherData weatherData = controller.getTempObj(cityName,dataBase,tempTypeF);
        System.out.println("Finished.");
        System.out.println(weatherData.getCity()+": "+weatherData.getTemp());

        WeatherAdapter weatherAdapter = new WeatherAdapter();
        WeatherElements newRes=weatherAdapter.marshal(weatherData);
        return newRes;
    }

}
