package com.epam.weathergetter.service;

import com.epam.weathergetter.wrapper.WeatherElements;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding
public interface IService {
    @WebMethod
    public WeatherElements getObjectTempByCityName(String cityName, boolean tempTypeF);
}
